import ffmpeg, subprocess, re, os, mutagen, json
from Tagger import extractCover

def mergeSilenceAndOtherIntervals(lengthStarts:list, lengthEnds:list, silenceStarts:list, silenceEnds:list):
    mergedStarts, mergedEnds, isSilenceStarts, isSilenceEnds = list(), list(), list(), list()

    #silenceEnds.reverse()

    for i, (lengthStart, lengthEnd) in enumerate(zip(lengthStarts, lengthEnds)):
        for j in range(len(silenceStarts)):
            if abs(lengthStart - silenceStarts[j]) < 3 * (i + 6):
                mergedStarts.append(silenceStarts[j])
                isSilenceStarts.append(True)
                silenceStarts.pop(j)
                break

        if len(mergedStarts) != i+1:
            mergedStarts.append(lengthStart)
            isSilenceStarts.append(False)
        
        for j in range(len(silenceEnds)):
            if abs(lengthEnd - silenceEnds[j]) < 3 * (i + 6):
                mergedEnds.append(silenceEnds[j])
                isSilenceEnds.append(True)
                silenceEnds.pop(j)
                break

        if len(mergedEnds) != i+1:
            mergedEnds.append(lengthEnd)
            isSilenceEnds.append(False)
    
    for i in range(1, len(mergedStarts) - 1):
        if(isSilenceStarts[i] == False and isSilenceEnds[i - 1] == True):
            mergedStarts[i] = mergedEnds[i - 1]
            isSilenceStarts[i] = True

        if(isSilenceEnds[i] == False and isSilenceStarts[i + 1] == True):
            mergedEnds[i] = mergedStarts[i + 1]
            isSilenceEnds[i] = True

    return mergedStarts, mergedEnds, isSilenceStarts, isSilenceEnds

def getChaptersIntervals(filepath):
    chapters = json.load( open(filepath) )
    starts, ends = [], []

    for chapter in chapters:
        starts.append(float(chapter['start_time']))
        ends.append(float(chapter['end_time']))

    return starts, ends

def getTrackLengthIntervals(release):
        pos = 0
        starts, ends = list(), list()

        for song in release.tracklist:
            semicolon = song.duration.find(':')

            try:
                duration = int(song.duration[0:semicolon]) * 60 + int(song.duration[semicolon+1:len(song.duration)]) + 1
            except ValueError:
                duration = 0
            else:
                starts.append(pos)
                ends.append(duration + pos)
                pos += duration

        return starts, ends

def getCorrectSilenceLength(tracks:int, file:str):
    i=0.05
    add = 0.10

    while(i<2):
        starts, ends = getSilenceIntervals(i, file)
        
        if(len(starts) <= tracks):
            return starts, ends
        
        i += add
        add += 0.05

    return [], [], 0

def getSilenceIntervals(silenceLen, file):
    if file == "" or silenceLen<0:
        print("bad file or silenceLen")
        return 0

    process = (ffmpeg
        .input(file)
        .filter('silencedetect',noise='{}dB'.format(-50), d=silenceLen)
        .output('-', format='null')
        .compile()
    )

    output = subprocess.Popen(process, stderr=subprocess.PIPE).communicate()[1].decode('utf-8').splitlines()

    silenceStartReg = re.compile(' silence_start: (?P<start>[0-9]+(\.?[0-9]*))$')
    silenceEndReg   = re.compile(' silence_end: (?P<end>[0-9]+(\.?[0-9]*)) ')

    starts, ends = [], []

    for line in output:
        silenceStartMatch = silenceStartReg.search(line)
        silenceEndMatch = silenceEndReg.search(line)

        if silenceStartMatch:
            ends.append(float(silenceStartMatch.group('start')))

            if len(starts) == 0:
                starts.append(0.0)
    
        elif silenceEndMatch:
            starts.append(float(silenceEndMatch.group('end')))

    if len(starts) == 0:
        starts.append(0.0)

    if len(starts)>len(ends):
        ends.append(getAudioLength(file))

    for i, (start, end) in reversed(list(enumerate(zip(starts, ends)))):
        if end - start < 20:
            starts.pop(i)
            ends.pop(i)

    return starts, ends

def cutAudio(starts, ends, file:str, release, infoType:str):
    if infoType == "Discogs":
        folder = characterFilter(release.artists[0].name) + ' - ' + characterFilter(release.title)
    elif infoType == "Chapters":
        fileStart = file.rfind(os.sep)
        if fileStart == -1:
            fileStart = file.rfind("/")
            if fileStart == -1:
                fileStart = 0

        fileEnd = file.rfind('.')
        if fileEnd == -1:
            fileEnd == len(file)
        folder = characterFilter(file[fileStart:fileEnd])

    ext = file[file.rfind('.')+1:len(file)]
    files = list()

    try:
        os.mkdir(folder)
    except OSError as error:
        pass

    trackList = None
    if infoType == "Discogs":
        trackList = release.tracklist
    elif infoType == "Chapters":
        trackList = release

    print(release)

    for i, song in enumerate(trackList):
        while True:
            try:
                if infoType == "Discogs":
                    outputFile = characterFilter(folder) + os.sep + characterFilter(song.position + ' - ' + song.title + '.' + ext)
                elif infoType == "Chapters":
                    outputFile = characterFilter(folder) + os.sep + characterFilter(str(i+1) + ' - ' + song["title"] + '.' + ext)

                if starts[i] == ends[i]:
                    continue

                stream = (ffmpeg
                            .input(file, ss=starts[i], to=ends[i])
                            .output(outputFile, vn=None, c='copy') #map_metadata=0
                            .overwrite_output()
                         )

                command = stream.compile()
                
                stream.run(capture_stdout=True, capture_stderr=True)
                
                files.append([outputFile, song])

                break
            except ffmpeg.Error as e:
                print('command:')
                for w in command:
                    print(w, end=' ')
                print('stderr:', e.stderr.decode('utf-8'))
                break
    
    extractCover(file, folder + os.sep + 'cover.png')

    return files


def getAudioLength(file):
    mutagenObj = mutagen.File(file)

    return mutagenObj.info.length

def characterFilter(string:str):
    string = string.replace(':','')
    string = string.replace('\"','')
    string = string.replace('\'','')
    string = string.replace('?','')
    string = string.replace('/','')
    string = string.replace('\\','')

    return string

