from PyQt6.QtWidgets import QDialog, QDialogButtonBox, QVBoxLayout, QLabel, QPushButton
from PyQt6.QtCore import *

class ChooseString(QDialog):
    def __init__(self, title, msg, buttonStrings):
        super().__init__()

        self.setWindowTitle(title)

        self.layout = QVBoxLayout()
        
        msgObj = QLabel()
        msgObj.setText(msg)

        self.layout.addWidget(msgObj)

        self.buttonBox = QDialogButtonBox()

        for string in buttonStrings:
            button = QPushButton(string)
            button.clicked.connect(lambda state, x = string: self.finish(x))
            self.buttonBox.addButton(button, QDialogButtonBox.ButtonRole.AcceptRole)
        
        self.buttonBox.accepted.connect(self.accept)
        self.buttonBox.rejected.connect(self.reject)

        self.layout.addWidget(self.buttonBox)
        
        self.setLayout(self.layout)
    
    def finish(self, string):
        self.string = string

        self.accept()