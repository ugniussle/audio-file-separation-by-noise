import os
os.environ['PYGAME_HIDE_SUPPORT_PROMPT'] = ''
from pygame.mixer import music
from pygame import mixer
# import pydub
from PyQt6.QtWidgets import *
from PyQt6 import QtCore
from PyQt6.QtGui import QGuiApplication
from AudioProcessing import *
from Tagger import setTags
from ChooseString import ChooseString

class SetTrackIntervals(QDialog):
    def __init__(self, title:str, filepath:str, release:any, areDurationsMissing:bool, dir:str, infoType:str, chaptersFile:str = ""):
        super().__init__()
        
        self.setWindowTitle(title)
        
        self.layout = QVBoxLayout()

        self.filepath = filepath
        self.release = release
        self.infoType = infoType

        hLayout = QHBoxLayout()
        
        hLayout.addWidget(QLabel('Set the correct song intervals.'))
        
        icon = self.style().standardIcon(QStyle.StandardPixmap.SP_MediaVolume)
        pixmap = icon.pixmap(16,16)
        iconLabel = QLabel()
        iconLabel.setPixmap(pixmap)

        hLayout.addWidget(iconLabel)

        self.volumeSlider = QSlider(QtCore.Qt.Orientation.Horizontal)
        self.volumeSlider.setMinimum(0)
        self.volumeSlider.setMaximum(100)
        self.volumeSlider.sliderReleased.connect(self.changeVolume)
        self.volumeSlider.setValue(40)
        
        hLayout.addWidget(self.volumeSlider)

        self.layout.addLayout(hLayout)

        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self.incrementSlider)
        
        self.clock = QLabel('0:00:000')

        self.seekSlider = QSlider(QtCore.Qt.Orientation.Horizontal)
        self.seekSlider.setMinimum(0)
        self.songLength = int(getAudioLength(self.filepath))
        self.seekSlider.setMaximum(self.songLength * 1000)
        self.seekSlider.sliderReleased.connect(self.seek)

        hLayout = QHBoxLayout()
        hLayout.addWidget(self.clock)
        hLayout.addWidget(self.seekSlider)
        self.layout.addLayout(hLayout)

        playButton = QPushButton('Play / Pause')
        playButton.clicked.connect(self.play)

        decrementSeek, incrementSeek = QPushButton('-'), QPushButton('+')
        decrementSeek.clicked.connect(lambda state, x=False:self.seek(increment=x))
        incrementSeek.clicked.connect(lambda state, x=True:self.seek(increment=x))
        decrementSeek.setMaximumWidth(20)
        incrementSeek.setMaximumWidth(20)

        hLayout = QHBoxLayout()
        hLayout.addWidget(decrementSeek)
        hLayout.addWidget(playButton)
        hLayout.addWidget(incrementSeek)
        self.layout.addLayout(hLayout)

        mixer.init()

        #pydub.AudioSegment.from_file(filepath).export("playback.ogg", "ogg", parameters=["-preset", "ultrafast"])

        #music.load("playback.ogg")
        music.load(filepath)

        music.set_volume(0.4)

        music.play()
        music.pause()

        starts, ends, isSilenceStarts, isSilenceEnds = self.generateIntervals(areDurationsMissing, infoType, self.release, chaptersFile)

        scrollArea = QScrollArea()
        groupBox = QGroupBox()
        groupBox.setLayout(self.createTrackList(self.release, starts, ends, isSilenceStarts, isSilenceEnds, infoType))
        scrollArea.setWidget(groupBox)
        scrollArea.setMaximumHeight(300)
        scrollArea.setMinimumWidth(380)

        self.layout.addWidget(scrollArea)

        cutAudio = QPushButton('Cut the audio')

        cutAudio.clicked.connect(self.cutAudio)

        self.layout.addWidget(cutAudio)

        self.layout.setSizeConstraint(QLayout.SizeConstraint.SetMaximumSize)

        self.setLayout(self.layout)

    def generateIntervals(self, areDurationsMissing, infoType, release, chaptersFile):
        methods = ['Silence', 'Youtube chapters']
        if areDurationsMissing == False:
            methods.append('Track length')

        if infoType == "Chapters":
            chapterStarts, chapterEnds = getChaptersIntervals(chaptersFile)
            silenceStarts, silenceEnds = getSilenceIntervals(0.05, self.filepath)
            starts, ends, isSilenceStarts, isSilenceEnds = mergeSilenceAndOtherIntervals(chapterStarts, chapterEnds, silenceStarts, silenceEnds)
        else:
            dlg = ChooseString('Interval getting method','Choose the method to calculate song intervals', methods)

            dlg.exec()

            if dlg.string == 'Track length':
                starts, ends = getTrackLengthIntervals(release)
                isSilenceStarts, isSilenceEnds = [], []

            elif dlg.string == 'Silence' and areDurationsMissing == True:
                starts, ends = getCorrectSilenceLength(len(release.tracklist), self.filepath)
                isSilenceStarts, isSilenceEnds = [], []

            elif dlg.string == 'Silence' and areDurationsMissing == False:
                silenceStarts, silenceEnds = getSilenceIntervals(0.05, self.filepath)
                lengthStarts, lengthEnds = getTrackLengthIntervals(release)
                starts, ends, isSilenceStarts, isSilenceEnds = mergeSilenceAndOtherIntervals(lengthStarts, lengthEnds, silenceStarts, silenceEnds)

            elif dlg.string == 'Youtube chapters':
                chaptersFile = QFileDialog.getOpenFileName(self, 'Select file with youtube chapters', dir)[0]

                chapterStarts, chapterEnds = getChaptersIntervals(chaptersFile)
                silenceStarts, silenceEnds = getSilenceIntervals(0.05, self.filepath)
                starts, ends, isSilenceStarts, isSilenceEnds = mergeSilenceAndOtherIntervals(chapterStarts, chapterEnds, silenceStarts, silenceEnds)

        return starts, ends, isSilenceStarts, isSilenceEnds

    def play(self, args):
        if music.get_busy() == False:
            music.unpause()
            self.timer.start(1)
        
        elif music.get_busy() == True:
            music.pause()
            self.timer.stop()

    def seek(self, objName = None, increment:bool = None):
        if objName != None:
            try:
                time = self.clockToSeconds(self.findChild(QLineEdit, objName).text())
                self.seekSlider.setValue(int(time * 1000))
            except:
                print('seek obj not found')
                return

        elif increment != None:
            if increment == True:
                value = 1000

            elif increment == False:
                value = -1000

            modifiers = QGuiApplication.keyboardModifiers()

            if modifiers == QtCore.Qt.KeyboardModifier.ControlModifier:
                value *= 10
            
            elif modifiers == QtCore.Qt.KeyboardModifier.ShiftModifier:
                value /= 10

            time = (self.seekSlider.sliderPosition() + value) / 1000
            self.seekSlider.setValue(int(time * 1000))

        else:
            time = self.seekSlider.sliderPosition() / 1000

        wasMusicPlaying = music.get_busy()

        music.stop()
        try:
            music.play(start = time)
        except:
            music.play(start = self.songLength)
        
        if wasMusicPlaying == False:
            music.pause()
            self.timer.stop()
        
        self.clock.setText(self.secondsToClock(time))

    def changeVolume(self):
        volume = self.volumeSlider.sliderPosition() / 100
        music.set_volume(volume)

    def incrementSlider(self):
        self.seekSlider.setValue(self.seekSlider.value() + 1)
        self.clock.setText( self.secondsToClock(self.seekSlider.value() / 1000) )

    def createTrackList(self, release, starts, ends, isSilenceStarts, isSilenceEnds, infoType):
        tracksLayout = QVBoxLayout()

        if infoType == "Discogs":
            list = release.tracklist
        elif infoType == "Chapters":
            list = release
        for i, song in enumerate(list):

            if infoType == "Discogs":
                trackGroup = QGroupBox(song.position + ' ' + song.title)
            elif infoType == "Chapters":
                trackGroup = QGroupBox(str(i+1) + ' ' + song['title'])


            gridLayout = QGridLayout()

            gridLayout.addWidget(QLabel('start'), 0, 0)
            gridLayout.addWidget(QLabel('end'), 0, 2)

            start, end = QLineEdit(), QLineEdit()
            
            try:
                start.setText(self.secondsToClock(starts[i]))
            except:
                pass
            
            try:
                end.setText(self.secondsToClock(ends[i]))
            except:
                pass

            startName, endName = 'start'+str(i), 'end'+str(i)
            
            start.setObjectName(startName), end.setObjectName(endName)
            start.setMaximumWidth(74),      end.setMaximumWidth(74)
            gridLayout.addWidget(start, 0, 1)
            gridLayout.addWidget(end, 0, 3)

            if len(isSilenceStarts) != 0:
                if(isSilenceStarts[i] == True):
                    start.setStyleSheet('border: 2px solid green')
                if(isSilenceEnds[i] == True):
                    end.setStyleSheet('border: 2px solid green')

            seekStart, seekEnd = QPushButton('seek'), QPushButton('seek')
            setStart,  setEnd =  QPushButton('set'),  QPushButton('set')

            seekStart.clicked.connect(lambda state, x = startName: self.seek(x))
            seekEnd.clicked.connect(lambda state, x = endName: self.seek(x))
            setStart.clicked.connect(lambda state, x = startName: self.setInterval(x))
            setEnd.clicked.connect(lambda state, x = endName: self.setInterval(x))

            gridLayout.addWidget(seekStart, 1, 0)
            gridLayout.addWidget(seekEnd, 1, 2)
            gridLayout.addWidget(setStart, 1, 1)
            gridLayout.addWidget(setEnd, 1, 3)

            trackGroup.setLayout(gridLayout)

            tracksLayout.addWidget(trackGroup)
        
        tracksLayout.setSizeConstraint(QLayout.SizeConstraint.SetFixedSize)
        return tracksLayout

    def setInterval(self, objName):
        time = self.clockToSeconds(self.clock.text())

        self.findChild(QLineEdit,objName).setText(self.secondsToClock(time))

    def cutAudio(self):
        starts, ends = self.getIntervals()

        files = cutAudio(starts, ends, self.filepath, self.release, self.infoType)

        setTags(self.release, files, self.infoType)

        music.stop()

        self.accept()

    def getIntervals(self):
        starts, ends = [], []
        startRE = QtCore.QRegularExpression('start*')
        endRE = QtCore.QRegularExpression('end*')

        for start, end in zip(self.findChildren(QLineEdit,startRE), self.findChildren(QLineEdit,endRE)):
            starts.append(self.clockToSeconds(start.text()))
            ends.append(self.clockToSeconds(end.text()))

        return starts, ends

    def secondsToClock(self, seconds):
        miliseconds = int((seconds%60)*1000)%1000
        return str(int(seconds/60)) + ':' + self.leadingZeroes(seconds % 60, 10) + str(int(seconds%60)) + ':' + self.leadingZeroes(miliseconds, 100) + str(miliseconds)

    def clockToSeconds(self, clock:str):
        try:
            semicolon1 = clock.find(':')
            semicolon2 = clock.find(':',semicolon1+1)

            return (int(clock[0:semicolon1]) * 60) + (int(clock[semicolon1+1:semicolon2])) + (int(clock[semicolon2+1:len(clock)]) / 1000)
        except:
            return 0

    def leadingZeroes(self, seconds, measurement):
        leadingZeroes = ''

        while seconds < measurement and measurement >= 10:
            leadingZeroes = leadingZeroes + '0'
            measurement /= 10

        return leadingZeroes
