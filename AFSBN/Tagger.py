import mutagen, discogs_client
from os import remove
import ffmpeg
from mutagen.flac import Picture
import filetype
from base64 import b64encode

def setTags(release:discogs_client.Release, files:list, infoType:str):
    filename:str
    song:discogs_client.Track

    for i, (filename, song) in enumerate(files):
        try:
            testFile(filename)
        except FileNotFoundError:
            print(filename,'not found')
            continue
        except AttributeError:
            print('cannot access file', filename+',', 'it will not be tagged.')
            continue

        file = mutagen.File(filename)

        if infoType == "Discogs":
            file.tags['title'] = song.title

            file.tags['tracknumber'] = song.position

            if release.title != None:
                file.tags['album'] = release.title

            if  release.artists[0] != None: 
                file.tags['albumartist'] = release.artists[0].name

            try:
                if  song.artists[0] != None:
                    file.tags['artist'] = song.artists[0].name
            except IndexError:
                if  release.artists[0] != None:
                    file.tags['artist'] = release.artists[0].name

            if release.year != None:
                file.tags['date'] = str(release.year)
                
        if infoType == "Chapters":
            file.tags['title'] = song["title"]
            file.tags['tracknumber'] = str(i+1)

        file.save()

    return

def testFile(filename:str):
    file = mutagen.File(filename)
    file.tags
    file.save()

def cutThumbnails(entries:dict, saveOriginalThumbnail:bool):
    try:
        remove('_cover.png')
    except FileNotFoundError:
        pass

    try:
        remove('cover.png')
    except FileNotFoundError:
        pass

    for entry in entries:
        try:
            testFile(entry['filepath'])
        except FileNotFoundError:
            continue
        except AttributeError:
            print('cannot access file '+entry['filepath']+ '. Cover will not be cut.')
            continue

        coverBack = extractCover(entry['filepath'], 'cover.png')

        width, height = getPicDim(coverBack)

        coverFront = cropImage(coverBack, width, height, '_cover.png')
        
        saveCoverToFile(coverFront, coverBack, entry['filepath'], height, width, saveOriginalThumbnail)

        remove('_cover.png')
        remove('cover.png')

def extractCover(file:str, output:str):
    stream = ffmpeg.input(file)
    stream = ffmpeg.output(stream, output, vframes=1, format='image2', vcodec='copy')
    stream = ffmpeg.overwrite_output(stream)

    ffmpeg.run(stream,quiet=True)

    return output

def getPicDim(file:str):
    probe = ffmpeg.probe(file)
    video_stream = next((stream for stream in probe['streams'] if stream['codec_type'] == 'video'), None)

    width =  int(video_stream['width'])
    height = int(video_stream['height'])

    return width, height

def cropImage(file:str, width:int, height:int, output:str):
    stream = ffmpeg.input(file)
    stream = ffmpeg.crop(stream, (width-height) / 2, 0, height, height)
    stream = ffmpeg.output(stream,output)
    stream = ffmpeg.overwrite_output(stream)

    ffmpeg.run(stream, quiet=True)

    return output

def saveCoverToFile(coverFront:str, coverBack:str, file:str, height:int, width:int, saveOriginalThumbnail:bool):
    file = mutagen.File(file)

    pic = Picture()
    pic.mime = 'image/%s' % filetype.guess(coverFront)
    
    coverList = list()

    with open(coverFront,'rb') as thumbfile:
        pic.data = thumbfile.read()
    pic.type = 3
    pic.width  = height
    pic.height = height

    coverList.append(b64encode(pic.write()).decode('ascii'))

    if(saveOriginalThumbnail == True):
        with open(coverBack,'rb') as thumbfile:
            pic.data = thumbfile.read()
        
        pic.type = 4
        pic.width  = width
        pic.height = height

        coverList.append(b64encode(pic.write()).decode('ascii'))

    file.tags['METADATA_BLOCK_PICTURE'] = coverList

    file.save()
