from PyQt6.QtWidgets import QDialog, QDialogButtonBox, QVBoxLayout, QLabel
from PyQt6.QtCore import *

class Dialog(QDialog):
    def __init__(self, title, msg, buttons=2):
        super().__init__()

        self.setWindowTitle(title)

        self.layout = QVBoxLayout()
        
        msgObj = QLabel()
        msgObj.setText(msg)

        self.layout.addWidget(msgObj)

        if buttons == 2:
            button = QDialogButtonBox.StandardButton.Yes | QDialogButtonBox.StandardButton.No

        elif buttons == 1:
            button = QDialogButtonBox.StandardButton.Ok
        
        if buttons > 0:
            self.buttonBox = QDialogButtonBox(button)
            self.buttonBox.accepted.connect(self.accept)
            self.buttonBox.rejected.connect(self.reject)

            self.layout.addWidget(self.buttonBox)
        
        self.setLayout(self.layout)
