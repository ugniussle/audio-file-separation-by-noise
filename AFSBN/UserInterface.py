from PyQt6.QtWidgets import *
from PyQt6.QtGui import QDesktopServices
from PyQt6.QtCore import QUrl
import sys, os, discogs_client
from Dialog import Dialog
from SetTrackIntervals import SetTrackIntervals
from SelectRelease import SelectRelease
from MainWindow import Ui_MainWindow
from discogs_client import exceptions, Client
from pygame.mixer import music

import json

class MainWindow(QMainWindow, Ui_MainWindow):
    def __init__(self, *args, obj=None, **kwargs):
        super(MainWindow, self).__init__(*args,**kwargs)
        
        self.setupUi(self)

        self.lastOpenedDir = self.loadLoc()

        self.SelectFile.clicked.connect(self.selectFile)
        self.File.textEdited.connect(self.setFile)

        self.SelectChapters.clicked.connect(self.selectChaptersFile)
        self.ChaptersFile.textEdited.connect(self.setChaptersFile)

        self.SearchDiscogs.clicked.connect(self.searchDiscogs)
        self.ChaptersContinue.clicked.connect(self.chaptersContinue)

        self.filename = self.File.text()

    def setFile(self, text):
        self.filename = text

    def setChaptersFile(self, text):
        self.chaptersFile = text
    
    def selectFile(self):
        self.filename = QFileDialog.getOpenFileName(self, 
                                                    'Select File',
                                                    self.lastOpenedDir)[0]

        self.lastOpenedDir=self.filename[0 : (self.filename.rfind('/') + 1)]
        self.saveLoc(self.lastOpenedDir)

        self.File.setText(self.filename)

    def selectChaptersFile(self):
        self.chaptersFile = QFileDialog.getOpenFileName(self, 
                                                    'Select File',
                                                    self.lastOpenedDir)[0]

        self.ChaptersFile.setText(self.chaptersFile)


    def searchDiscogs(self):
        try:
            client = Client('AFSBN/0.5', user_token=self.APIKey.text())
        
            searchQuery:str = self.SearchQuery.text()
            searchQuery = searchQuery.strip()

            if searchQuery != "":
                res = client.search(self.SearchQuery.text(), type='release')
            else:
                dlg = Dialog('Search query needed', 'Please provide a search query matching\nthe album of the file.', 1)
                return dlg.exec()

            res.page(0)
        except exceptions.HTTPError:
            dlg = Dialog(title = 'User token required',
                         msg = "To use this program you need to generate a user token at "
                               "https://www.discogs.com/settings/developers. \nPress \'Ok\' to visit this link.",
                         buttons = 1)
            
            if dlg.exec() == 1:
                QDesktopServices.openUrl(QUrl('https://www.discogs.com/settings/developers'))
            return

        if(len(res.page(0)) == 0):
            Dialog('Release not found', 'No release matching the search was found.', 1).exec()
            return

        dlg = SelectRelease('Select the release', res.page(0))

        if(dlg.exec() == 1):
            release:discogs_client.Release = res.page(0)[dlg.selected]
            areDurationsMissing = dlg.missingDurations
        else:
            release = None

        if release == None:
            return False

        dlg = SetTrackIntervals('Set intervals', self.filename, release, areDurationsMissing, self.lastOpenedDir, "Discogs")
        
        music.stop()
        
        return dlg.exec()

    def chaptersContinue(self):
        release = json.load(open(self.chaptersFile))
        
        dlg = SetTrackIntervals('Set intervals', self.filename, release, False, self.lastOpenedDir, "Chapters", self.chaptersFile)
        
        music.stop()
        
        return dlg.exec()
    
    def saveLoc(self, path):
        try:
            file = open('location', 'wt')

            file.seek(0)
            file.truncate()
            file.write(path)

            file.close()
        except:
            pass

    def loadLoc(self):
        try:
            file = open('location', 'rt')
            return file.readline()
        except:
            return os.getcwd()

def start():
    app = QApplication(sys.argv)

    window = MainWindow()

    window.show()
    app.exec()