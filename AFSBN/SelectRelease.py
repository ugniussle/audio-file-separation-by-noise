import json
from PyQt6.QtWidgets import *
from PyQt6.QtGui import QPixmap
import requests
from os import remove
from Dialog import Dialog
from discogs_client import exceptions

class SelectRelease(QDialog):
    def __init__(self, title, resPage):
        super().__init__()
        
        self.setWindowTitle(title)

        self.resPage = resPage

        self.layout = QVBoxLayout()

        msgObj = QLabel('Select the appropriate release and press \'Continue\'')
        
        self.layout.addWidget(msgObj)

        if(len(self.resPage) >= 10):
            showCount = 10
        else:
            showCount = len(self.resPage)

        self.table = QTableWidget(showCount,2)
        self.table.verticalHeader().setDefaultSectionSize(100)
        self.table.horizontalHeader().setDefaultSectionSize(100)
        self.table.verticalHeader().hide()
        self.table.horizontalHeader().hide()
        self.table.setMinimumWidth(800)
        self.table.setMinimumHeight(600)
        self.table.setSelectionMode(QAbstractItemView.SelectionMode.SingleSelection)
        self.table.setFrameShape(QFrame.Shape.NoFrame)
        self.table.setStyleSheet('QTableWidget{ background-color:rgb(240, 240, 240);outline:0; } QTableWidget::item:selected{ background-color:rgba(150, 0, 0, 0.5);border:0; }')
        
        self.setReleaseData(self.resPage)

        self.table.setShowGrid(False)
        self.layout.addWidget(self.table)
        self.table.setColumnWidth(1,680)

        button = QPushButton('Continue')
        button.clicked.connect(self.continueProgram)

        self.layout.addWidget(button)
        
        self.setLayout(self.layout)

    def continueProgram(self):
        self.selected = self.table.selectedIndexes()[0].row()
        
        self.missingDurations = False

        release = self.resPage[self.selected]
        for song in release.tracklist:
            if song.duration == '':
                dlg = Dialog('Missing durations', 'This release has missing song durations, which means the song interval algorithm will have a harder time. Do you wish to continue with your selection?')
                
                if dlg.exec() == 1:
                    self.missingDurations = True
                    break

                else:
                    return

        self.accept()

    def setReleaseData(self, resPage):
        for i, release in enumerate(resPage):
            if(i == 10):
                break

            label = QLabel()

            try:
                label.setPixmap(QPixmap(self.getCover(release.thumb)).scaledToWidth(100))
            except:
                pass
        
            self.table.setCellWidget(i,0,label)

            labelText = ''
            try:
                labelText += str(release.artists[0].name) + ' — '
            except json.JSONDecodeError:
                labelText += '? — '
            except exceptions.HTTPError:
                labelText += '? — '

            try:
                labelText += release.title + '   '
            except json.JSONDecodeError:
                labelText += '?   '

            try:
                if(release.year != 0):
                    labelText += '   year:' + str(release.year) + '   '
            except json.JSONDecodeError:
                labelText += '?   '

            try:
                labelText += 'tracks: ' + str(len(release.tracklist))
            except json.JSONDecodeError:
                labelText += 'tracks: ?'

            label = QLabel(labelText)
            label.setWordWrap(True)
            font = label.font()
            font.setPointSize(14)
            label.setFont(font)
            
            self.table.setCellWidget(i,1,label)

        remove('cover.jpg')

    def getCover(self, link):
        session = requests.session()

        try:
            response = session.get(link)
        except:
            response = ''

        if response != '':
            with open('cover.jpg','wb') as file:
                for chunk in response:
                    file.write(chunk)

            return 'cover.jpg'
        else:
            return None
