## audio file separation by noise

Python Qt GUI program to split an album in one file to multiple files using discogs api and ffmpeg.

# Usage

1. Open the program.
2. Put in your discogs API key from https://www.discogs.com/settings/developers (you need an account) into the API key field.
3. Select the file that you want to split with the 'Select File...' button.
4. Write the album name, artist or other album identifiers into the search field.
5. Press 'Search'.
6. Select the appropriate release for your file and press 'Continue'.
7. Choose the song interval calculation method (99% of the time select 'Silence', 'Track length' is only if you know that there are no silence periods between any of the songs).
8. In the set intervals window you can play the original file. <br />
Press 'Play / Pause' to play or pause playback. <br />
Press the '-' and '+' buttons to seek 1 second back or forward. Holding the Shift key reduces this seek to 0.1 seconds, Control increases it to 10 seconds. <br />
On an individual song element you see it's track number and title on the outline. Here 'start' shows when the song starts in the original file and same with 'end'. Pressing seek seeks to the timestamp above the button in the player. Pressing set sets the timestamp to the current playback position of the player. <br />
If a timestamp's border is green, that means it was set by the silence search algorithm and is correct 99% of the time. If not, it was calculated from the tracks durations and is incorrect, but still somewhat close to the actual time. <br />
9. When you are happy with all the intervals, press 'Cut the audio' button. This will create a folder in the same directory as the program and put the individual song files into it.
10. If you want to cut an additional file, go back to step 4.
