import subprocess, os

compileMainWindow = 'pyuic6 AFSBN' + os.sep +'mainwindow.ui -o AFSBN' + os.sep +'MainWindow.py'
run = 'python AFSBN' + os.sep +'main.py'

try:
    subprocess.run(compileMainWindow.split(' '))
except:
    print('UI compilation failed')

try:
    subprocess.run(run.split(' '))
except:
    print('running failed')